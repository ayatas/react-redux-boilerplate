// @flow
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { css } from 'aphrodite';

import s from './style';

type Props = {
  history: History,
};

class MainContainer extends React.Component<Props> {
  render() {
    return(
      <div>
        <ConnectedRouter history={this.props.history}>
          <div>
            <Switch>                      
            </Switch>
          </div>
        </ConnectedRouter>
      </div>
    );
  }
}

export default MainContainer;
