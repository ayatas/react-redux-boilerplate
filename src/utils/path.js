import qs from 'qs';
import { isArray } from './convertCase';

export function addParamsToUrl(url, params = {}) {
  const keys = Object.keys(params);
  if (keys.length === 0) {
    return url;
  }
  let requestUrl = url + '?';
  keys.forEach((key, index, self) => {
    let query = `${key}=`;
    if (isArray(params[key])) {
      query += params[key].join(',');
    } else {
      query += params[key]
    }
    requestUrl += query;
    if (index < self.length -1) {
      requestUrl += '&';
    }
  });
  return requestUrl;
}

export function getQueries(search) {
  const queries = qs.parse(search.replace(/\?/g, ''));
  const keys = Object.keys(queries);
  let newQueries = {};
  keys.forEach(key => {
    newQueries[key] = (queries[key].includes(',')) ? queries[key].split(',') : queries[key];
  });
  return newQueries;
}
