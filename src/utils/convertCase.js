function snakeToCamelStr(str) {
  return str.replace(/_./g, (s) => s.charAt(1).toUpperCase());
}
function camelToSnakeStr(str) {
  return str.replace(/([A-Z])/g, (s) => `_${s.charAt(0).toLowerCase()}`);
}

export function isObject(obj) {
  return (obj instanceof Object && !(obj instanceof Array)) ? true : false;
}
export function isArray(obj) {
  return (obj instanceof Array) ? true : false;
}

export function snakeToCamelObj(obj) {
  let newObj;
  if (isArray(obj)) {
    newObj = obj.map(item => snakeToCamelObj(item));
  } else if (isObject(obj)) {
    newObj = {};
    for (const key in obj) {
      if(!obj.hasOwnProperty(key)) continue;
      const item = obj[key];
      newObj[snakeToCamelStr(key)] = snakeToCamelObj(item);
    }
  } else {
    newObj = obj;
  }
  return newObj;
}

export function camelToSnakeObj(obj) {
  let newObj;
  if (isArray(obj)) {
    newObj = obj.map(item => camelToSnakeObj(item));
  } else if (isObject(obj)) {
    newObj = {};
    for (const key in obj) {
      if(!obj.hasOwnProperty(key)) continue;
      const item = obj[key];
      newObj[camelToSnakeStr(key)] = camelToSnakeObj(item);
    }
  } else {
    newObj = obj;
  }
  return newObj;
}