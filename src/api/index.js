// @flow
import { camelToSnakeObj, snakeToCamelObj } from '../utils/convertCase';
import { addParamsToUrl } from '../utils/path';

class Api {
  static async request(path: string, method: string, { params = null, data = null }: { params?: ?Object, data?: ?Object } = {}) {
    let url = process.env.API_END_POINT + path;
    if (params) {
      url = addParamsToUrl(url, camelToSnakeObj(params));
    }
    const headers = {
      'Content-Type': 'application/json',
    };
    const body = (data) ? JSON.stringify(camelToSnakeObj(data)) : null;
    const options = {
      method,
      headers,
      mode: 'cors',
      credentials: 'include',
      body,
    };
    try {
      const response = await fetch(url, options);
      const json = snakeToCamelObj(await response.json());
      return json;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}

export default Api;