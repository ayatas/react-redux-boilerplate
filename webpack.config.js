const path = require('path');
const webpack = require('webpack');
require('dotenv').config();

module.exports = {
  entry: path.resolve(__dirname + '/src/index.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: [/\.js$/],
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css/,
        loaders: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { url: false }
          }
        ]
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, 'public'),
    port: 8080,
    host: "0.0.0.0",
    inline: true
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'API_END_POINT': JSON.stringify(process.env.API_END_POINT)
      }
    })
  ],
  devtool: 'source-map'
};

