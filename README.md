# How to Use
```sh
// Install npm modules
yarn install

// Install flow-typed
yarn global add flow-typed
flow-typed install

// Start webpack-dev-server(http://localhost:3000/)
yarn run start
```
